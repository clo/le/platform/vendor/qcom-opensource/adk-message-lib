/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef EXAMPLES_BUTTON_MANAGER_BUTTON_MANAGER_H_
#define EXAMPLES_BUTTON_MANAGER_BUTTON_MANAGER_H_

#include "../../include/adk/message-service/adk-message-service.h"

namespace adk {
namespace examples {
/**
 * @brief This is a simple example of a adk-protobuffer service server that emits ADK signals. It is 
 * implemented using the ADKMessageService which uses ADKMessageService::AdkIpcService. The AdkIpcService 
 * is a subclass of ipc::Emitter and ipc::Listener. In this Button Manager example, the Emitter is exercised via 
 * ADKMessageService::Send method.
 * */


class ButtonMgrService  {
 public:
  ButtonMgrService():message_service("button_mgr"){}
  bool mainService();
  adk::msg::AdkMessageService message_service;
 
 private:
  //proto service
  adk::msg::AdkMessage adk_message_press;
  adk::msg::AdkMessage adk_message_release;

private:
  bool shutting_down_ = {false};
};
}  // namespace examples
}  // namespace adk

#endif  // EXAMPLES_BUTTON_MANAGER_BUTTON_MANAGER_H_
