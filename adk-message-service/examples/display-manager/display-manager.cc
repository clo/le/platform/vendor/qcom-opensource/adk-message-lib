/*
Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include "display-manager.h"

#include <core/posix/signal.h>
#include <unistd.h>
#include <map>
#include <string>
#include <tuple>
#include "adk/log.h"
using adk::msg::AdkMessage;
using adk::msg::AdkMessageService;

namespace adk {
namespace examples {

DisplayMgrService::~DisplayMgrService() = default;

bool DisplayMgrService::Start() {
  ADK_LOG_INFO("DisplayManager::Start()");
  this->message_service.Initialise();
  this->message_service.Subscribe(adk::msg::AdkMessage::kButtonPressed,
          [](adk::msg::AdkMessage& dbus_received_message) {
                      dbus_received_message.PrintDebugString();
          });

  this->message_service.Subscribe(adk::msg::AdkMessage::kButtonReleased,
          [](adk::msg::AdkMessage& dbus_received_message) {
                      dbus_received_message.PrintDebugString();
          });

}

}  // namespace examples
}  // namespace adk

int main(int, char**) {
  // Instantiate a SignalTrap object to handle blocking of the enumerated
  // list of system termination signals we wish to block. The signaltrap
  // object will allow this program to exit gracefully when intercepting
  // those signals.
  auto signaltrap = core::posix::trap_signals_for_process(
    {core::posix::Signal::sig_int, core::posix::Signal::sig_term});

  // Register a callback function with the signaltrap object to be notified
  // whenever one of the blocked signals raised by the operating system
  // is trapped. The callback function will exit the trap listening loop
  // so that this program can perform any required cleanup of the running
  // service before terminating.
  signaltrap->signal_raised().connect(
      [signaltrap](core::posix::Signal) { signaltrap->stop(); });

  // Instantiate a DisplayMgrService (dbus-cpp service to access D-Bus IPC).
  adk::examples::DisplayMgrService display_manager_service;

  ADK_LOG_INFO("Display Manager Service Start Status: %d", display_manager_service.Start());

  // Start listening for system incoming signals and block program
  // until signaltrap->stop() is called.
  signaltrap->run();

  // Stop execution of server service.
  // service->StopServerService();

  ADK_LOG_INFO("display-manager (example) exiting...");
  return EXIT_SUCCESS;

};
