/*
Copyright (c) 2018-2019, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <unistd.h>  // sleep()
#include <iostream>
#include <list>
#include <string>
#include <fstream>
#include <ctime>
#include <iostream>
#include "adk/message-service/adk-message-service.h"
#include <signal.h>

using namespace std;
using std::cout;
using std::endl;
using std::cerr;
using adk::msg::AdkMessage;
using adk::msg::AdkMessageService;
using adk::msg::msg_name_to_oneof_id;
using google::protobuf::Reflection;
using google::protobuf::Descriptor;
using google::protobuf::FieldDescriptor;
#define DBUS_SESSION_FILE "/tmp/dbus-session"
const std::string HELPER = R"(Usage: adk-message-monitor
e.g.
  -Subscribe to all messages
    adk-message-monitor -a
  -Subscribe to messages from specific group
    adk-message-monitor -g led)";

ostream & cout_timestamp() {
 std::time_t result = std::time(nullptr);
 int now = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
 std::string time = std::asctime(std::localtime(&result));
 time.erase(std::remove(time.begin(), time.end(), '\n'), time.end());
 return(std::cout << "[ms:" << now << " " << time << "]");
}

int main(int argc, char* argv[]) {
  bool aflag = false;
  bool gflag = false;
  std::string gvalue;
  int c;
  opterr = 0;

  if (argc < 2) {
    cout << "Please use valid monitor options." << endl;
    cout << HELPER << endl;
    exit(EXIT_FAILURE);
  }

  // Set dBus-session
  ifstream dBusfile(DBUS_SESSION_FILE);
  std::string file_data;
  if (dBusfile.is_open()) {
    while (getline (dBusfile,file_data) ) {
      string var = file_data.substr(0, file_data.find("="));
      string value = file_data.substr(file_data.find("=") + 1);
      const char *env = var.c_str();
      const char *set = value.c_str();
      setenv(env, set, 0);
    }
    dBusfile.close();
  } else {
    std::cerr << "Unable to open file - " << DBUS_SESSION_FILE << ", export DBUS_SESSION_BUS_ADDRESS manually" << endl;
}

  while ((c = getopt(argc, argv, "hag:")) != -1) switch (c) {
      case 'h':
       cout << HELPER << endl;
       std::exit(EXIT_SUCCESS);
      case 'a':
        aflag = true;
        break;
      case 'g':
        gflag = true;
        gvalue = optarg;
        break;
      case '?':
        if (optopt == 'g')
          fprintf(stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint(optopt))
          fprintf(stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
        return 1;
      default:
        std::exit(EXIT_FAILURE);
    }

  sigset_t signal_set;
  if (sigemptyset(&signal_set) < 0) {
    std::cout << "ERROR: Unable to create process signal mask" << std::endl;
    std::exit(EXIT_FAILURE);
  }
  if (sigaddset(&signal_set, SIGINT) < 0) {
    std::cout << "ERROR: Unable to add to process signal mask" << std::endl;
    std::exit(EXIT_FAILURE);
  }
  if (sigprocmask(SIG_BLOCK, &signal_set, nullptr) < 0) {
    std::cout << "ERROR: Unable to set process signal mask" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  AdkMessageService adk_message_service("adk_message_monitor");
  streambuf* orig_buf = cout.rdbuf();
  cout.rdbuf(NULL);
  adk_message_service.Initialise();
  cout.rdbuf(orig_buf);

  if (aflag) {
    for (auto& map_entry : msg_name_to_oneof_id) {
      auto oneof_id = map_entry.second;
      adk_message_service.Subscribe(oneof_id,
                                    [](AdkMessage& received_message) {
                                      cout << "Message Received ";
                                      cout_timestamp() << std::endl;
                                      received_message.PrintDebugString();
                                    });
    }
  } else if (gflag) {
    adk_message_service.Subscribe(gvalue,
                                  [](AdkMessage& received_message) {
                                    cout << "Message Received ";
                                    cout_timestamp() << std::endl;
                                    received_message.PrintDebugString();
                                    });
  } else {
    std::list<std::string> message_name_list;
    for (int i = 1; i < argc; i++) {
      message_name_list.push_back(std::string(argv[i]));
    }
    for (auto message_name : message_name_list) {
      auto it = msg_name_to_oneof_id.find(message_name);
      if (it == msg_name_to_oneof_id.end()) {
        std::cerr << "WARNING: Message \"" << message_name << "\" not found."
                  << std::endl;
        continue;
      }
      auto oneof_id = it->second;
      adk_message_service.Subscribe(oneof_id,
                                    [](AdkMessage& received_message) {
                                    cout << "Message Received ";
                                    cout_timestamp() << std::endl;
                                    received_message.PrintDebugString();
                                    });
    }
  }

  bool success;
  bool waiting_for_signals = true;
  while (waiting_for_signals) {
    // Block until a signal arrives
    int signal_number;
    int error = sigwait(&signal_set, &signal_number);

    // Check there was no error
    if (error) {
      cout_timestamp() << "WARN: Error " << error << " while waiting for process signals" << std::endl;
      success = false;
      break;
    }

    // Check which signal it was
    switch (signal_number) {
      case SIGINT:
        // Exit loop, terminate gracefully
        waiting_for_signals = false;
        break;

      default:
        cout_timestamp() << "ERROR: Received unexpected process signal" << std::endl;
        waiting_for_signals = false;
        success = false;
        break;
    }
  }
  cout_timestamp() << "Exiting Monitor" << std::endl;
  return 0;
}
