/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <google/protobuf/text_format.h>
#include <unistd.h>  // sleep()
#include <iostream>
#include <regex>
#include "adk/message-service/adk-message-service.h"
#include <fstream>
#include <string>
#include <algorithm>
#include <functional>
#define DBUS_SESSION_FILE "/tmp/dbus-session"
const std::string HELPER = R"(Usage: adk-message-send <Text-format protocol message>
e.g.
"user {\n id: 123 extra { gender: MALE language: 'en' }\n}"
 adk-message-send 'led_indicate_direction_pattern{pattern:<pattern> direction:<angle_of_arrival>}'
 adk-message-send 'connectivity_wifi_connect {ssid:"<ssid>" password:"<password>" homeap:true}'

other options:
 -l (list all the ADK messages)
 -m message (list the message fields))";

using namespace std;
using adk::msg::AdkMessage;
using adk::msg::AdkMessageService;
using google::protobuf::Reflection;
using google::protobuf::Descriptor;
using google::protobuf::FieldDescriptor;


// Create the message in the user friendly format
void eraseAllSubStr(std::string & mainStr, const std::string & toErase) {
  size_t pos = std::string::npos;
  while ((pos  = mainStr.find(toErase) )!= std::string::npos) {
    mainStr.erase(pos, toErase.length());
  }
}

void eraseSubStringsPre(std::string & mainStr, const std::vector<std::string> & strList) {
  for (std::vector<std::string>::const_iterator it = strList.begin(); it != strList.end(); it++) {
    eraseAllSubStr(mainStr, *it);
  }
}

std::string replaceSubString(std::string & mainStr, std::string & replaceStr, std::string & pattern){
  std::regex re(pattern);
  std::string output = std::regex_replace(mainStr, re, replaceStr);
  return (output);
}
void userFriendlyMessage(std::string output, std::string replaceWith) {
  // Erase unwanted string
  output.erase (0,8);
  // Replace "=" with ":" to generate user message format
  size_t position = 0;
  std::string search("=");
  std::string replace(":");
  while ((position = output.find(search, position)) != std::string::npos) {
    output.replace(position, search.length(), replace);
    position += replace.length();
  }

  // Remove all the dev related details from message
  const std::vector<std::string> list = { "optional", "required","char","double","string","int32","int64", "int", "bool",";"};
  eraseSubStringsPre(output, list);
  std::string pattern("([0-9]+)");
  std::string replacement("xx ");
  output = replaceSubString(output,replacement,pattern);
  output = output.substr(output.find_first_of(" \t")+1);

  // Construction of user format message
  output ="adk-message-send '"+replaceWith+" "+output+"'";

  // Remove unwanted newlines and spaces
  output.erase(std::remove(output.begin(), output.end(), '\n'), output.end());
  std::string space("    ");
  std::string space_replacement("");
  output = replaceSubString(output,space_replacement,space);

  //Print the message
  cout <<"- User message format: \n *Replace xx with user input (For string type-> use \"\" around input and for bool type-> only use true or false) \n \n"<< output << endl;
}

int main(int argc, char* argv[]) {
  if (argc < 2) {
    cout << "Please enter valid message to send." << endl;
    cout << HELPER << endl;
    exit(EXIT_FAILURE);
  }
  // Set dBus-session
  ifstream dBusfile (DBUS_SESSION_FILE);
  string file_data;
  if (dBusfile.is_open()) {
    while ( getline (dBusfile,file_data) ) {
      string var = file_data.substr(0, file_data.find("="));
      string value = file_data.substr(file_data.find("=") + 1);
      const char *env = var.c_str();
      const char *set = value.c_str();
      setenv(env, set, 0);
    }
    dBusfile.close();
  } else {
    std::cerr << "Unable to open file - " << DBUS_SESSION_FILE << ", export DBUS_SESSION_BUS_ADDRESS manually" << endl;
  }

  std::string payload_string = argv[1];
  int c;
  AdkMessage message;
  auto desc = message.GetDescriptor();

  while ((c = getopt(argc, argv, "hlm:")) != -1) switch (c) {
    case 'h': {
      cout << HELPER << endl;
      std::exit(EXIT_SUCCESS);
    }
    case 'm': {
      cout << optarg << " message details: \n" << endl;
      bool found = false;
      for (auto entry: adk::msg::msg_name_to_oneof_id) {
        auto message_case = entry.second;
        auto dispaly_message = desc->FindFieldByNumber(message_case);

        if (dispaly_message->name() == optarg) {
          string replaceWith = dispaly_message->name().c_str();
          string output = dispaly_message->message_type()->DebugString().c_str();
          cout <<"- Message fields type: \n \n"<< output << endl;
          userFriendlyMessage(output,replaceWith);
          found = true;
          break;
        }
      }
      if (!found) {
        cout << "Message: " << optarg << " not found, please use -l option to check list of ADK messages" << endl;
      }
      std::exit(EXIT_SUCCESS);
    }
    case 'l': {
      int msg_count =1;
      cout << "List of all the ADK Messages" << endl;
      for (auto entry: adk::msg::msg_name_to_oneof_id) {
        auto message_case = entry.second;
        auto dispaly_message = desc->FindFieldByNumber(message_case);
        cout << dispaly_message->name() << endl;
        msg_count = msg_count + 1;
      }
      cout << '\n' << "Total ADK Messages: " << msg_count << endl;
      std::exit(EXIT_SUCCESS);
    }
    default:
      std::exit(EXIT_SUCCESS);
    }

    AdkMessageService adk_message_service("adk_message_send");
    streambuf* orig_buf = cout.rdbuf();
    cout.rdbuf(NULL);
    adk_message_service.Initialise();
    cout.rdbuf(orig_buf);

    AdkMessage send_message;
    google::protobuf::TextFormat::ParseFromString(payload_string, &send_message);

    if (send_message.IsInitialized()) {
      std::cout << "Sending message:" << endl;
      send_message.PrintDebugString();
      cout.rdbuf(NULL);
      if (!adk_message_service.Send(send_message)) {
        std::cerr << "Failed to send" << std::endl;
      }
    } else {
      cout << "ERROR: Message not initialized" << endl;
    }
  return 0;
}
