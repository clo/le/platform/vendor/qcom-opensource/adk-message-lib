/*
Copyright (c) 2019, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/registry.hpp>
#include <xmlrpc-c/server_abyss.hpp>

#include <google/protobuf/text_format.h>
#include <unistd.h>
#include "adk/message-service/adk-message-service.h"
#include <fstream>
#include <string>
#include "msgqueue.h"

using namespace std;
using adk::msg::AdkMessage;
using adk::msg::AdkMessageService;
using google::protobuf::Reflection;
using google::protobuf::Descriptor;
using google::protobuf::FieldDescriptor;
using adk::msg::msg_name_to_oneof_id;

#define DBUS_SESSION_FILE "/tmp/dbus-session"
#define XMLRPC_LOG "/tmp/xmlrpc_log"
adk::MsgQueue<AdkMessage> message_queue_;
std::list<uint64_t> reponse_time_list;
/**
 * @brief method to send and receive adk-messages remotely
 * @param[payload_string] adk-message
 * @param[monitor_time] timeout for adk-monitor
 * @return Array of all the adk-message received in the specified timeout
 */
class sendAdkMessages : public xmlrpc_c::method {
public:
    sendAdkMessages() : adk_message_service("adk_test_server") {
        // Set the DBUS_SESSION_BUS_ADDRESS
        ifstream dBusfile (DBUS_SESSION_FILE);
        string file_data;
        // File open will fail if file doesn't exist
        // Message will be printed to export DBUS_SESSION_BUS_ADDRESS manually
        if (dBusfile.is_open()) {
          while ( getline (dBusfile,file_data) ) {
            string var = file_data.substr(0, file_data.find("="));
            string value = file_data.substr(file_data.find("=") + 1);
            const char *env = var.c_str();
            const char *set = value.c_str();
            setenv(env, set, 0);
          }
          dBusfile.close();
        } else {
          std::cerr << "Unable to open file - " << DBUS_SESSION_FILE << ", export DBUS_SESSION_BUS_ADDRESS manually" << endl;
        }
        adk_message_service.Initialise();
    }
    //adk::MsgQueue<AdkMessage> message_queue_;
    bool subscribe = true;
    AdkMessageService adk_message_service;
     
    /**
     * @brief XML-RPC call arrives via an Abyss server.
     *        which is available to the client via XML-RPC execute() method.
     * @param paramList (payload_string, monitor_time)
     * @return retvalP
     */
    void
    execute(xmlrpc_c::paramList const& paramList,
            xmlrpc_c::value *   const  retvalP) {

        if (subscribe) {
          // Subscribe just once, when server is started
          // Not every time, when method is called remotely
          subscribe = false;
          for (auto& map_entry : msg_name_to_oneof_id) {
            auto oneof_id = map_entry.second;
            adk_message_service.Subscribe(oneof_id,
                                        [this](AdkMessage received_message) {
                                          cout << "Message Received ";
                                          received_message.PrintDebugString();
                                          message_queue_.add(received_message);
                                          uint64_t t_end = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
                                          cout << "Message response received time(ms):" << t_end << endl;
                                          reponse_time_list.push_front(t_end);
                                        });

          }
        }
        #define PARAMETER_1 0
        #define TOTAL_PARAMETERS 1

        std::string payload_string(paramList.getString(PARAMETER_1));

        paramList.verifyEnd(TOTAL_PARAMETERS);

        AdkMessage send_message;
        google::protobuf::TextFormat::ParseFromString(payload_string, &send_message);

        uint64_t t_start = 0;
        // Send the adk-message requested by client
        if (send_message.IsInitialized()) {
          std::cout << "Sending message:" << endl;
          send_message.PrintDebugString();
          t_start = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
          std::cout << "Message sent time(ms): " << t_start << endl;
          if (!adk_message_service.Send(send_message)) {
            std::cerr << "Failed to send" << std::endl;
          }
        } else {
          cout << "ERROR: Message not initialized" << endl;
        }
        *retvalP = xmlrpc_c::value_i8(t_start);
    }
};

class poll : public xmlrpc_c::method {
public:
    poll() { }
    void
    execute(xmlrpc_c::paramList const& paramList,
            xmlrpc_c::value *   const  retvalP) {
        vector<xmlrpc_c::value> arrayData;
        std::string output;
        int queue_size = message_queue_.size();  
        std::cout << "Total message received " << "-> " << queue_size << endl;

        if (queue_size >= 1) {
          for (int index = 0; index<=queue_size-1; index++) {
            AdkMessage message_response = message_queue_.remove();
            std::string msg;
            message_response.SerializeToString(&msg);
            hexify(msg, output);
            arrayData.push_back(xmlrpc_c::value_string(output));
            //  Get the message received time and add it to list  to send back to client
            if (!reponse_time_list.empty()){
              arrayData.push_back(xmlrpc_c::value_i8(reponse_time_list.front()));
              reponse_time_list.pop_front();
            } else {
              arrayData.push_back(xmlrpc_c::value_i8(0));
            }
          }
        } else {
          arrayData.push_back(xmlrpc_c::value_string("None"));
        }
        // Make an XML-RPC array to send it back to client
        xmlrpc_c::value_array messageArray(arrayData);
        *retvalP = xmlrpc_c::value_array(messageArray);
    }

    /**
     * @brief Hexify the serialized string to send it over XMLRPC
     *        This is required to send the non prinatble character over XMLRPC
     * @param Serialized msg
     * @param Output
     */
    void hexify(std::string& msg, std::string& output) {
        static const char* const lut = "0123456789ABCDEF";
        size_t len = msg.length();
        if (len >= 1) {
          output.reserve(2 * len);
          for (size_t i = 0; i < len; ++i) {
            const unsigned char c = msg[i];
            output.push_back(lut[c >> 4]);
            output.push_back(lut[c & 15]);
          }

        }
    }
};

int 
main(int           const argc, 
     const char ** const argv) {

    xmlrpc_c::registry myRegistry;

    xmlrpc_c::methodPtr const sendAdkMessagesP(new sendAdkMessages);
    xmlrpc_c::methodPtr const pollP(new poll);


    myRegistry.addMethod("send_message", sendAdkMessagesP);
    myRegistry.addMethod("poll_responses", pollP);

    int TCP_PORT = 5000;
    if (argc == 2) {
        TCP_PORT = atoi(argv[1]);
    }
    std::cout << "adk-test-server started on port -> " << TCP_PORT << endl;

    xmlrpc_c::serverAbyss myAbyssServer(
        myRegistry,
        TCP_PORT,   // TCP port on which to listen
        XMLRPC_LOG  // Log file
        );

    myAbyssServer.run();

    return 0;
}
