/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <adk/message-service/adk-message-service.h>
#include <gtest/gtest.h>
#include <list>
#include <memory>
#include "msgqueue.h"

using adk::msg::AdkMessageService;

class StaticServiceTests : public ::testing::Test {
 public:
  static void SetUpTestCase() {
    message_service_ptr = new AdkMessageService("message_service_test");
    ASSERT_TRUE(message_service_ptr->Initialise());
  }
  static void TearDownTestCase() { delete message_service_ptr; }
  void TearDown() {
    for (auto subscription_token : handler_id_list) {
      EXPECT_TRUE(message_service_ptr->Unsubscribe(subscription_token));
    }
  }
  adk::MsgQueue<AdkMessage> message_queue_;
  static AdkMessageService* message_service_ptr;
  std::list<adk::msg::SubscriptionToken> handler_id_list;
};

AdkMessageService* StaticServiceTests::message_service_ptr;
TEST_F(StaticServiceTests, BasicSendAndReceiveMessage) {
  adk::msg::SubscriptionToken subscription_token;
  message_service_ptr->Subscribe(AdkMessage::kAllplayPause,
                                 [this](AdkMessage received_command) {
                                   message_queue_.add(received_command);
                                 },
                                 &subscription_token);
  handler_id_list.push_back(subscription_token);

  AdkMessage message;
  message.mutable_allplay_pause();
  EXPECT_TRUE(message_service_ptr->Send(message));

  AdkMessage received_message = message_queue_.remove();
  EXPECT_EQ(AdkMessage::kAllplayPause, received_message.adk_msg_case());
}

TEST_F(StaticServiceTests, BasicSendAndReceiveMessageRetest) {
  adk::msg::SubscriptionToken subscription_token;
  message_service_ptr->Subscribe(AdkMessage::kAllplayNext,
                                 [this](AdkMessage received_command) {
                                   message_queue_.add(received_command);
                                 },
                                 &subscription_token);
  handler_id_list.push_back(subscription_token);

  AdkMessage message;
  message.mutable_allplay_next();
  EXPECT_TRUE(message_service_ptr->Send(message));

  AdkMessage received_message = message_queue_.remove();
  EXPECT_EQ(AdkMessage::kAllplayNext, received_message.adk_msg_case());
}

TEST_F(StaticServiceTests, SendAndReceiveMessageData) {
  adk::msg::SubscriptionToken subscription_token;
  message_service_ptr->Subscribe(AdkMessage::kConnectivityWifiConnect,
                                 [this](AdkMessage received_command) {

                                   message_queue_.add(received_command);
                                 },
                                 &subscription_token);
  handler_id_list.push_back(subscription_token);

  AdkMessage message;
  auto wifi_connect_message = message.mutable_connectivity_wifi_connect();
  wifi_connect_message->set_ssid("TestSSID");
  wifi_connect_message->set_password("TestPassword");
  wifi_connect_message->set_homeap(true);
  EXPECT_TRUE(message_service_ptr->Send(message));

  AdkMessage received_message = message_queue_.remove();
  EXPECT_STRCASEEQ("TestSSID",
                   received_message.connectivity_wifi_connect().ssid().c_str());
  EXPECT_STRCASEEQ(
      "TestPassword",
      received_message.connectivity_wifi_connect().password().c_str());
  EXPECT_TRUE(received_message.connectivity_wifi_connect().homeap());
}

TEST_F(StaticServiceTests, CallingSubscribeWithDefaultArgument) {
  EXPECT_TRUE(message_service_ptr->Subscribe(
      AdkMessage::kButtonPressed, [this](AdkMessage received_command) {
        message_queue_.add(received_command);
      }));

  AdkMessage message;
  auto button_pressed_message = message.mutable_button_pressed();
  button_pressed_message->set_button("CAMFOCUS");
  EXPECT_TRUE(message_service_ptr->Send(message));

  AdkMessage received_message = message_queue_.remove();
  EXPECT_STRCASEEQ("CAMFOCUS",
                   received_message.button_pressed().button().c_str());
}

TEST_F(StaticServiceTests, Unsubscribe) {
  adk::msg::SubscriptionToken subscription_token1;
  adk::msg::SubscriptionToken subscription_token2;

  EXPECT_TRUE(
      message_service_ptr->Subscribe(AdkMessage::kConnectivityWifiEnable,
                                     [this](AdkMessage received_command) {
                                       message_queue_.add(received_command);
                                     },
                                     &subscription_token1));

  EXPECT_TRUE(
      message_service_ptr->Subscribe(AdkMessage::kConnectivityBtEnable,
                                     [this](AdkMessage received_command) {
                                       message_queue_.add(received_command);
                                     },
                                     &subscription_token2));
  AdkMessage bt_message;
  AdkMessage wifi_message;

  bt_message.mutable_connectivity_bt_enable();
  wifi_message.mutable_connectivity_wifi_enable();

  message_service_ptr->Unsubscribe(subscription_token1);

  EXPECT_TRUE(message_service_ptr->Send(bt_message));
  EXPECT_TRUE(message_service_ptr->Send(wifi_message));

  AdkMessage received_message = message_queue_.remove();
  EXPECT_EQ(AdkMessage::kConnectivityBtEnable, received_message.adk_msg_case());
  EXPECT_EQ(0, message_queue_.size());
}

int main(int argc, char** argv) {
  testing::InitGoogleTest(&argc, argv);

  return RUN_ALL_TESTS();
}
