/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef MESSAGE_SERVICE_ADK_MESSAGE_SERVICE_H_
#define MESSAGE_SERVICE_ADK_MESSAGE_SERVICE_H_

#include <memory>
#include <string>
#include "adk/ipc/serviceemitter.h"
#include "adk/ipc/servicelistener.h"
#include "adk/messages/messages.h"

using adk::msg::AdkMessage;
namespace adk {
namespace msg {

/**
 * @brief Typedef for the message callback for the subscription methods
 */
typedef std::function<void(AdkMessage &)> MsgCallback_t;
typedef typename ipc::Listener::DBusAdkMessageSignal::SubscriptionToken
    SubscriptionToken;
/**
 * @class AdkMessageService
 * @brief Wrapper class for the IPC that allows the use of the protobuffer
 * AdkMessage system. The InitialiseIPC() function must be called before making
 * any calls to the send or subscription methods
 */
class AdkMessageService {
 private:
  struct Private;
  std::unique_ptr<Private> self_;

 public:
  /**
   * @brief Constructor for the ADK Message Service wrapper class
   * @param[in] interface_name The name on the IPC. This name will be prefixed
   * by com.qualcomm.qti.adk. and the suffixed by listener and emitter
   * respectively
   */
  explicit AdkMessageService(const std::string &interface_name);
  virtual ~AdkMessageService();

  // Delete Copy Constructor
  AdkMessageService(const AdkMessageService &) = delete;
  // Disallowing assignment operations
  AdkMessageService &operator=(const AdkMessageService &) = delete;
  /**
   * @brief InitialiseIPC() must be called after the AdkMessageService
   * construction before using any of the methods. Failure to do so may result
   * in uknown behaviour.
   * @return True if the ADK Message Service could be started successfully.
   */
  virtual bool Initialise();

  /**
   * @brief Send an AdkMessage on the IPC
   * @param[in] adk_message The AdkMessage to send on the IPC
   * @return False if the message could not be sent or is uknown to the library.
   */
  virtual bool Send(const AdkMessage &adk_message);

  /**
   * @brief register a callback function for all messages on the dbus
   * @param callback Function pointer for the callback.
   * @param[out] handler_id Returned handler ID for the
   * listener callback. Used to unregister the callback from the IPC
   * @return False if the callback could not be registered.
   */
  virtual bool Subscribe(const MsgCallback_t &callback,
                 SubscriptionToken *subscription_token = nullptr);

  /**
   * @brief register a callback function for all message from a group.
   * @param[in] group Group name to subscribe the call back to.
   * @param callback Function pointer for the callback.
   * @param[out] handler_id Returned handler ID for the
   * listener callback. Used to unregister the callback from the IPC
   * @return False if the callback could not be registered.
   */
  virtual bool Subscribe(std::string group, const MsgCallback_t &callback,
                 SubscriptionToken *subscription_token = nullptr);

  /**
   * @brief register a callback function for a specific message on the IPC
   * @param[in] msg_index The AdkMessage case for the message to subscribe to.
   * @param callback Function pointer for the callback.
   * @param[out] handler_id Returned handler ID for the
   * listener callback. Used to unregister the callback from the IPC
   * @return False if the callback could not be registered.
   */
  virtual bool Subscribe(AdkMessage::AdkMsgCase msg_index,
                 const MsgCallback_t &callback,
                 SubscriptionToken *subscription_token = nullptr);

  /**
   * @brief Unregister a message subscription using the Handler ID for that
   * subscription.
   * @param[in] handler_id The handler ID of the registered subscrition to
   * unregister.
   * @return True if the callback was successfully unregistered.
   */
  virtual bool Unsubscribe(SubscriptionToken subscription_token);
};

}  // namespace msg
}  // namespace adk

#endif  // MESSAGE_SERVICE_ADK_MESSAGE_SERVICE_H_
