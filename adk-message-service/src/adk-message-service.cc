/*
Copyright (c) 2018, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "adk/message-service/adk-message-service.h"

#include <core/posix/signal.h>
#include <unistd.h>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <thread>
#include <tuple>
#include "adk/ipc/servicefactory.h"
#include "msgqueue.h"
using std::cout;
using std::endl;
using adk::msg::oneof_id_to_dbus_args;
using adk::msg::AdkMessage;

namespace adk {
namespace msg {
/**
 * @class AdkIpcService
 * @brief Internal service class that implements the IPC.
 */
class AdkIpcService : public ipc::Emitter, public ipc::Listener {
 public:
  /** shared pointer for ADK IPC service */
  typedef std::shared_ptr<AdkIpcService> Ptr;

  /**
   * @brief Constructor for the IPC service
   * @param[in] emitter_name The emitter name on the IPC bus
   * @param[in] listener_name The listener name on the IPC bus
   */
  AdkIpcService(std::string emitter_name, std::string listener_name);
  virtual ~AdkIpcService();

  /**
   * @brief Emit a ADK message on the Adk IPC message bus.
   * @param[in] adk_message The ceomple adk_message to be sent on the IPC.
   * @return False if the message is uknown to the ipc or if the message is
   * not initialised.
   */
  bool emit_ipc_msg(const AdkMessage& adk_message);

  /**
   * @brief register a callback function for all messages on the dbus
   * @param callback Function pointer for the callback.
   * @param[out] subscription_token Returned Subscription token for the
   * listener callback. Used to unregister the callback from the IPC
   * @return False if the callback could not be registered.
   */
  bool subscribe_ipc_msg(
      const MsgCallback_t& callback,
      DBusAdkMessageSignal::SubscriptionToken* subscription_token);

  /**
   * @brief register a callback function for all message from a group.
   * @param[in] group Group name to subscribe the call back to.
   * @param callback Function pointer for the callback.
   * @param[out] subscription_token Returned Subscription token for the
   * listener callback. Used to unregister the callback from the IPC
   * @return False if the callback could not be registered.
   */
  bool subscribe_ipc_msg(
      std::string group, const MsgCallback_t& callback,
      DBusAdkMessageSignal::SubscriptionToken* subscription_token);

  /**
   * @brief register a callback function for a specific message on the IPC
   * @param[in] msg_index The AdkMessage case for the message to subscribe to.
   * @param callback Function pointer for the callback.
   * @param[out] subscription_token Returned Subscription token for the
   * listener callback. Used to unregister the callback from the IPC
   * @return False if the callback could not be registered.
   */
  bool subscribe_ipc_msg(
      AdkMessage::AdkMsgCase msg_index, const MsgCallback_t& callback,
      DBusAdkMessageSignal::SubscriptionToken* subscription_token);

 private:
  bool StartServerService() override;
  bool StartSignalListeners() override;

  /**
   * @brief Send a message on the IPC using the three message arguments.
   * @param[in] arg0 The first argument (group name) of the adk IPC message.
   * @param[in] arg1 The second argument (message name) of the adk IPC
   * message.
   * @param[in] arg2 A serialised AdkMessage. This will be transported on the
   * IPC as a byte array variant with key "proto".
   * @return False if the message could not be sent.
   */
  bool send_to_ipc(std::string arg0, std::string arg1, std::string arg2);
};

AdkIpcService::AdkIpcService(std::string emitter_name,
                             std::string listener_name)
    : ipc::Emitter(emitter_name), ipc::Listener(listener_name) {
  cout << "Adk Ipc Service service started" << endl;
}

AdkIpcService::~AdkIpcService() = default;

bool AdkIpcService::StartServerService() {
  return true;
}

bool AdkIpcService::StartSignalListeners() {
  return true;
}

bool AdkIpcService::send_to_ipc(std::string arg0, std::string arg1,
                                std::string arg2) {
  std::map<std::string, AdkVariant> variant_map;

  std::vector<int8_t> data(arg2.begin(), arg2.end());

  variant_map["proto"] = AdkVariant::encode<std::vector<int8_t>>(data);

  auto proto_arg = std::make_tuple(arg0, arg1, variant_map);

  if (!EmitMessageSignal(proto_arg)) {
    // There has been an error while emitting this ADK signal.
    // For this example we just print out there has been an error.
    std::cout << "Error while emitting this Message signal" << std::endl;
    return false;
  }
  return true;
}

bool AdkIpcService::emit_ipc_msg(const AdkMessage& adk_message) {
  auto it = oneof_id_to_dbus_args.find(adk_message.adk_msg_case());

  // get the group and name for this message
  if (it == oneof_id_to_dbus_args.end()) {
    cout << "ERROR: unknown message type" << endl;
    return false;
  }
  if (!adk_message.IsInitialized()) {
    std::cerr << "ERROR: Message not initialised" << std::endl;
    return false;
  }
  std::string payload;
  payload = adk_message.SerializeAsString();

  return send_to_ipc(it->second.group, it->second.name, payload);
}

bool AdkIpcService::subscribe_ipc_msg(
    const MsgCallback_t& callback,
    DBusAdkMessageSignal::SubscriptionToken* subscription_token) {
  cout << "AdkIpcService::subscribe_ipc_msg. This "
          "Method is currently not supported"
       << endl;
  return true;
}

bool AdkIpcService::subscribe_ipc_msg(
    std::string group, const MsgCallback_t& callback,
    DBusAdkMessageSignal::SubscriptionToken* subscription_token) {
  cout << "Subscribing to group: " << group << endl;
  DBusMatchRuleArgs match1_arg{{0, group}};
  *subscription_token = ListenMessageSignalWithArgs(
      [this, callback](AdkMessageSignalPayload command) {
        std::cout << "AdkMessage received: " << std::get<0>(command) << " "
                  << std::get<1>(command) << " " << std::endl;

        std::map<std::string, AdkVariant> dbus_payload = std::get<2>(command);

        std::vector<int8_t> proto_array =
            dbus_payload.find("proto")->second.as<std::vector<int8_t>>();

        std::string proto_string(proto_array.begin(), proto_array.end());
        AdkMessage received_message;

        if (received_message.ParseFromString(proto_string)) {
          received_message.CheckInitialized();
          callback(received_message);
        } else {
          cout << "ERROR: could not parse message" << endl;
        }
      },
      match1_arg);
  return true;
}

bool AdkIpcService::subscribe_ipc_msg(
    AdkMessage::AdkMsgCase msg_index, const MsgCallback_t& callback,
    DBusAdkMessageSignal::SubscriptionToken* subscription_token) {
  auto it = oneof_id_to_dbus_args.find(msg_index);

  // get the group and name for this message
  if (it == oneof_id_to_dbus_args.end()) {
    cout << "ERROR: unknown message type" << endl;
    return false;
  } else {
    cout << "Subscribing to group: " << it->second.group
         << ", name: " << it->second.name << endl;
  }
  DBusMatchRuleArgs match1_arg{{0, it->second.group}, {1, it->second.name}};

  *subscription_token = ListenMessageSignalWithArgs(
      [this, callback](AdkMessageSignalPayload command) {
        std::cout << "AdkMessage received: " << std::get<0>(command) << " "
                  << std::get<1>(command) << " " << std::endl;

        std::map<std::string, AdkVariant> dbus_payload = std::get<2>(command);

        std::vector<int8_t> proto_array =
            dbus_payload.find("proto")->second.as<std::vector<int8_t>>();

        std::string proto_string(proto_array.begin(), proto_array.end());
        AdkMessage received_message;

        if (received_message.ParseFromString(proto_string)) {
          received_message.CheckInitialized();
          callback(received_message);
        } else {
          cout << "ERROR: could not parse message" << endl;
        }
      },
      match1_arg);

  return true;
}

struct AdkMessageService::Private {
  explicit Private(std::string interface_name) : kBusName(interface_name) {}

  std::vector<AdkIpcService::DBusAdkMessageSignal::SubscriptionToken>
      subscription_list;
  const std::string kBusName;
  std::shared_ptr<AdkIpcService> adk_ipc_service_;
};

AdkMessageService::AdkMessageService(const std::string& interface_name)
    : self_(new Private(interface_name)) {}

bool AdkMessageService::Initialise() {
  if (self_->kBusName.find("-") != std::string::npos) {
    std::cerr << "ERROR: Interface must not contain \'-\'." << std::endl;
    return false;
  }

  std::string emitter_name =
      "com.qualcomm.qti.adk." + self_->kBusName + ".emitter";
  std::string listener_name =
      "com.qualcomm.qti.adk." + self_->kBusName + ".listener";
  self_->adk_ipc_service_ =
      adk::ipc::announce_service_on_bus_with_listener<adk::ipc::Service,
                                                      AdkIpcService>(
          emitter_name, listener_name);
  if (self_->adk_ipc_service_ == nullptr) {
    return false;
  }

  if (!self_->adk_ipc_service_->adk::ipc::Listener::LaunchConnection()) {
    return false;
  }
  if (!self_->adk_ipc_service_->adk::ipc::Emitter::LaunchConnection()) {
    return false;
  }

  return true;
}

bool AdkMessageService::Send(const AdkMessage& adk_message) {
  return self_->adk_ipc_service_->emit_ipc_msg(adk_message);
}

bool AdkMessageService::Subscribe(const MsgCallback_t& callback,
                                  SubscriptionToken* subscription_token) {
  if (subscription_token == nullptr) {
    SubscriptionToken dummy_token;
    return self_->adk_ipc_service_->subscribe_ipc_msg(callback, &dummy_token);
  }
  return self_->adk_ipc_service_->subscribe_ipc_msg(callback,
                                                    subscription_token);
}

bool AdkMessageService::Subscribe(std::string group,
                                  const MsgCallback_t& callback,
                                  SubscriptionToken* subscription_token) {
  if (subscription_token == nullptr) {
    SubscriptionToken dummy_token;
    return self_->adk_ipc_service_->subscribe_ipc_msg(group, callback,
                                                      &dummy_token);
  }
  return self_->adk_ipc_service_->subscribe_ipc_msg(group, callback,
                                                    subscription_token);
}

bool AdkMessageService::Subscribe(AdkMessage::AdkMsgCase msg_index,
                                  const MsgCallback_t& callback,
                                  SubscriptionToken* subscription_token) {
  if (subscription_token == nullptr) {
    SubscriptionToken dummy_token;
    return self_->adk_ipc_service_->subscribe_ipc_msg(msg_index, callback,
                                                      &dummy_token);
  }
  return self_->adk_ipc_service_->subscribe_ipc_msg(msg_index, callback,
                                                    subscription_token);
}

bool AdkMessageService::Unsubscribe(SubscriptionToken subscription_token) {
  self_->adk_ipc_service_->UnregisterMessageListener(subscription_token);
  return true;
}

AdkMessageService::~AdkMessageService() = default;

}  // namespace msg
}  // namespace adk
