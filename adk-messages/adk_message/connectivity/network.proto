/*
 *Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.
 *
 *Redistribution and use in source and binary forms, with or without
 *modification, are permitted provided that the following conditions are
 *met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *    * Neither the name of The Linux Foundation nor the names of its
 *      contributors may be used to endorse or promote products derived
 *      from this software without specific prior written permission.
 *
 *THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 *WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 *ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 *BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 *BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 *OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 *IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

syntax = "proto2";

package adk.msg.connectivity;

/**
 * EthernetConnected - Ethernet connected indication
 * Connectivity manager shall send this message when Ethernet cable is connected to the device.
 */
message EthernetConnected {
}

/**
 * EthernetDisconnected - Ethernet disconnected indication
 * Connectivity manager shall send this message when Ethernet cable is disconnected from the device.
 */
message EthernetDisconnected {
}

/**
 * GetNetworkInfo - Get the current network state managed by Connectivity Manager
 */
message GetNetworkInfo {
}

/**
 * NetworkInfo - Network information managed by Connectivity Manager
 * Connectivity manager shall send this message when there's a change in the network (wifi/Ethernet) parameters or
 * when requested by any other application (by sending GetNetworkInfo)
 */
message NetworkInfo {
    message HomeConnection {
        optional string  connectivity    =  1;   /* ethernet/wifi/disabled */
        optional string  interface       =  2;   /* eth0/wlan0/wlan1/wlan2 interface */
        optional string  ip_address      =  3;   /* IP (V4) address of the link */
        optional string  mac_address     =  4;   /* MAC address for the interface */
        optional string  ssid            =  5;   /* Remote AP SSID, empty if connectivity is over Ethernet */
        optional string  bssid           =  6;   /* Remote AP BSSID, empty if connectivity is over Ethernet */
        optional string  encryption      =  7;   /* Encryption (OPEN/WEP/WPA2-PSK) of remote AP, empty if connectivity is over Ethernet */
        optional int32   rssi            =  8;   /* Signal strength for the connection, empty if connectivity is over Ethernet */
        optional int32   frequency       =  9;   /* Channel frequency (in MHz) for the connection, empty if connectivity is over Ethernet */
    }
    message MlanConnection {
        optional string  connectivity    =  1;    /* wifi/disabled */
        optional string  mode            =  2;    /* AP/STA mode for the MLAN*/
        optional string  interface       =  3;    /* wlan0/wlan1/wlan2 interface */
        optional string  ip_address      =  4;    /* IP (V4) address of the link */
        optional string  mac_address     =  5;    /* MAC address for the interface */
        optional string  ssid            =  6;    /* Remote AP SSID in STA mode, or of MLAN AP in AP mode */
        optional string  bssid           =  7;    /* Remote AP BSSID in STA mode, or of MLAN AP in AP mode */
        optional string  encryption      =  8;    /* Encryption (OPEN/WEP/WPA2-PSK) of remote AP in STA mode, or encryption in local MLAN AP in AP mode */
        optional int32   rssi            =  9;    /* Signal strength for the connection, empty if mode is AP */
        optional int32   frequency       = 10;    /* Channel frequency (in MHz) for the connection/AP */
    }
    message Onboarding {
        optional string  connectivity    =  1;    /* wifi/disabled */
        optional string  interface       =  2;    /* wlan0/wlan1/wlan2 interface */
        optional string  ip_address      =  3;    /* IP (V4) address of the link */
        optional string  mac_address     =  4;    /* MAC address for the interface */
        optional string  ssid            =  5;    /* SSID of Onboard AP */
        optional string  bssid           =  6;    /* BSSID of Onboard AP */
        optional string  encryption      =  7;    /* Encryption (OPEN/WEP/WPA2-PSK) of Onboard AP*/
        optional int32   frequency       =  8;    /* Channel frequency (in MHz) of Onboard AP */
    }
    message Bridge {
        optional string  interface       = 1;    /* bridge0/1 */
        optional string  ip_address      = 2;    /* IP address of bridge link */
    }
    optional HomeConnection home_connection = 1;
    optional MlanConnection mlan_connection = 2;
    optional Onboarding     onboarding      = 3;
    optional Bridge bridge = 4;
}
