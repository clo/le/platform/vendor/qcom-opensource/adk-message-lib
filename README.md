# adk-message-lib

Install Instuctions
=======
If you wish to use the library for a host machine and compile the messages,
you will need to install protobuffer library. protobuf 3.3.0 is what is used on current distribution

Download the package
https://github.com/protocolbuffers/protobuf/releases/download/v3.3.0/protobuf-cpp-3.3.0.tar.gz

Build and install:

```
tar zxf protobuf-cpp-3.3.0.tar.gz
cd protobuf-3.3.0/
./autogen.sh (you may need to run sudo apt-get install autoconf)
./configure
make
sudo make install
sudo ldconfig
protoc --version
libprotoc 3.3.0
```



Using the Library
=======

In CMake add the following:

pkg_check_modules(ADK_MESSAGE_SERVICE adk-message-service REQUIRED)

target_link_libraries (
...
${ADK_MESSAGE_SERVICE_LIBRARIES}
)

Using the Code
--------

You should only need to `#include "adk/message-service/adk-message-service.h"`
To send a message, the message must be constructed then be sent using the AdkMessageService object:
```
{
  AdkMessageService adk_message_service("message_on_ipc_bus"); // Must not contain "."

  AdkMessage message;

  // To set the message type and any payload information use the mutable_ methods
  auto button_pressed_message = message.mutable_button_pressed();
  button_pressed_message->set_button("VOLUP");

  if (message.IsInitialized()) {
    message.PrintDebugString();
    if (!adk_message_service.Send(send_message)) {
      std::cerr << "Failed to send" << std::endl;
    }
  } else {
    cout << "ERROR: Message not initialized" << endl;
  }
}

```

The process is similar for the subscription. You will need to pass a callback function pointer of type `void(adk::ipc::msg::AdkMessage &)`
The call to Subscribe() will return a subscription handle. This can be used to call the Unsubscribe() function.

```
adk::msg::SubscriptionToken subscription_token;
adk_message_service.Subscribe(
        adk::ipc::msg::AdkMessage::kButtonPressed,
        [](adk::ipc::msg::AdkMessage& received_message) {
          // Get the button of the received button pressed message.
          std::cout << received_message.button_pressed().button() << std::endl;
        },
        &subscription_token);

// To unsubscribe message handler
adk_message_service.Unsubscribe(subscription_token);

```

adk-message-send and adk-message-monitor
============
Make sure that the dbus session address is set before using the CLI tools

adk-message-send
-------------
adk-message-send can be used to send a message on the IPC.
The message must be known to the IPC.
It uses the text format version of the protobuffer messages example:
Example input: "user {\n id: 123 extra { gender: MALE language: 'en' }\n}"
more information: https://developers.google.com/protocol-buffers/docs/reference/cpp/google.protobuf.text_format#TextFormat.Parse

Example usage:
```
adk-message-send 'button_pressed {button:"VOLUP"}'
```

adk-message-monitor
-------------
Currently adk-message-monitor can only be used to monitor messages that are already known to the adk protobuffer messages library.

Usage:
adk-message-monitor <Message_name_1> <Message_name_2> ....

Options:
-g <group_name> Subscribe to all messages on the group. There is no name checking on the group name so ensure that it is a valid group name.
-a Subscribe to all messages known to the messages library.



